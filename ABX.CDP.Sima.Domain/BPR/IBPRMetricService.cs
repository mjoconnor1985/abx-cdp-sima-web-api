﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.Domain.BPR
{
    public interface IBPRMetricService
    {
        Task<IEnumerable<MetricSummary>> GetCortezProcessMetricsAsync();
    }
}
