﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABX.CDP.Sima.Domain.BPR
{
    public class MetricSummary
    {
        public string Name { get; set; }

        public string Unit { get; set; }

        public int DailyValue { get; set; }

        public int WeekToDateValue { get; set; }

        public int MonthToDateValue { get; set; }
    }
}
