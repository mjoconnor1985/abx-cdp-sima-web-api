﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using ABX.CDP.Sima.Data.BPR;
using ABX.CDP.Sima.Domain.BPR;
using ABX.CDP.Sima.Data.PI;

namespace ABX.CDP.Sima.Domain.BPR
{
    public class BPRMetricService : IBPRMetricService
    {

        #region Fields

        ISimaPiTagDataSource _simaTagDataSource;
        IBPRMetricDataSource _bprMetricDataSource;

        #endregion

        #region Constructors

        public BPRMetricService(ISimaPiTagDataSource simaTagDataSource, IBPRMetricDataSource metricDataSource )
        {
            _bprMetricDataSource = metricDataSource;
            _simaTagDataSource = simaTagDataSource;
        }

        #endregion

        #region Methods
        
        public async Task<IEnumerable<MetricSummary>> GetCortezProcessMetricsAsync()
        {
            var dbMetrics = await _bprMetricDataSource.GetCortezProcessMetricsAsync();
            return Mapper.Map<IEnumerable<ABX.CDP.Sima.Data.BPR.MetricSummary>, IEnumerable<MetricSummary>>(dbMetrics);
        }
        #endregion
    }
}
