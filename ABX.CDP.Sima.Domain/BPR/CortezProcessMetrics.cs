﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABX.CDP.Sima.Domain.BPR
{
    public class CortezProcessMetrics
    {
        public MetricSummary MillTonsProcessed { get; set; }

        public MetricSummary MillRecovery { get; set; }

        public MetricSummary LeachPercentageInSolution { get; set; }

        public MetricSummary LeachRecovery { get; set; }

        public MetricSummary TotalRecoveredLeach { get; set; }
    }
}
