﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABX.CDP.Sima.Domain.BPR
{
    public class MetricDetail
    {
        public string Name { get; set; }

        public string Unit { get; set; }

        public int Value { get; set; }
    }
}
