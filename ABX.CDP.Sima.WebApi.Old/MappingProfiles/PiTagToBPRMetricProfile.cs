﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ABX.CDP.BPR.Domain;
using ABX.CDP.BPR.Data.PI;

namespace ABX.CDP.BPR.WebApi.MappingProfiles
{
    public class PiTagToBPRMetricProfile: Profile
    {
        public PiTagToBPRMetricProfile()
        {
            CreateMap<PiTag, MetricDetail>();
        }
    }
}
