﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Swashbuckle.AspNetCore.Swagger;
using ABX.CDP.BPR.Data;
using ABX.CDP.BPR.Data.Metrics;
using ABX.CDP.BPR.Data.PI;
using ABX.CDP.BPR.Domain.Services;

namespace ABX.CDP.BPR.WebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            //3rd party middleware
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("bprapi", new Info { Title = "CDP PI Web API", Version = "v1" });
            });
            services.AddAutoMapper();

            services.AddSingleton<IConfigurationRoot>(Configuration);

            services.AddDbContext<SimaContext>(options => options
               .UseSqlServer(Configuration.GetConnectionString("Sima"), sqlOptions => sqlOptions.EnableRetryOnFailure()));

            //Data layer services
            services.AddScoped<IBPRMetricDataSource, BPRMetricDataSource>();
            services.AddScoped<ISimaPiTagDataSource, SimaPiTagDataSource>();

            //Domain layer services
            services.AddScoped<IBPRMetricService, BPRMetricService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            //loggerFactory.AddSeq(Configuration.GetSection("Seq"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                Authority = Configuration["Authentication:AzureAd:AADInstance"] + Configuration["Authentication:AzureAd:TenantId"],
                Audience = Configuration["Authentication:AzureAd:Audience"]
            });

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/bprapi/swagger.json", "CDP PI Web API");
            });
        }
    }
}
