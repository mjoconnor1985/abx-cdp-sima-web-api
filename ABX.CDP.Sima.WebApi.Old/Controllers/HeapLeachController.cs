﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ABX.CDP.BPR.Domain;
using ABX.CDP.BPR.Domain.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ABX.CDP.BPR.WebApi.Controllers
{
    [Route("api/cz/[controller]")]
    public class MetricsController : Controller
    {

        #region Fields

        IBPRMetricService _bprMetricSrv;

        #endregion
        public MetricsController(IBPRMetricService bprMetricSrv)
        {
            _bprMetricSrv = bprMetricSrv;
        }


        [HttpGet]
        [Route("")]
        async public Task<IActionResult> ProcessMetrics()
        {
            var processMetrics = await _bprMetricSrv.GetCortezProcessMetricsAsync();
            return Ok(processMetrics);
        }
    }
}
