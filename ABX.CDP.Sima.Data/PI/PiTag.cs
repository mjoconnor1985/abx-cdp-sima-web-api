﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABX.CDP.Sima.Data.PI
{
    public class PiTag
    {
        public DateTime Date { get; set; }

        public  int TagId { get; set; }

        public string Tag { get; set; }

        public string Description { get; set; }

        public string Value { get; set; }

        public string Unit { get; set; }

        public string UnitDescription { get; set; }
    }
}
