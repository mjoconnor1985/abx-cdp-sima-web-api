﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.Data.PI
{
    public interface ISimaPiTagDataSource
    {
        Task<IEnumerable<PiTag>> GetTagAsync(string tag, DateTime? starDate, DateTime? endDate);
    }
}
