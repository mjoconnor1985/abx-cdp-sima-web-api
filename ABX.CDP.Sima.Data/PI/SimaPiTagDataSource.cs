﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.Data.PI
{
    public class SimaPiTagDataSource : ISimaPiTagDataSource
    {
        #region Fields

        SimaContext _simaDb;

        #endregion

        #region Constructors

        public SimaPiTagDataSource(SimaContext simaContext)
        {
            _simaDb = simaContext;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<PiTag>> GetTagAsync(string tag, DateTime? starDate, DateTime? endDate)
        {
            return await _simaDb.PiTags.Where(p => p.Tag == tag && p.Date >= starDate && p.Date <= endDate).OrderBy(p => p.Date).ToListAsync();
        }

        #endregion
    }
}
