﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.Data.BPR
{
    public interface IBPRMetricDataSource
    {
        Task<IEnumerable<MetricSummary>> GetCortezProcessMetricsAsync();
    }
}
