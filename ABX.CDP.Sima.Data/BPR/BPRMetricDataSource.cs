﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.Data.BPR
{
    public class BPRMetricDataSource : IBPRMetricDataSource
    {
        #region Fields

        SimaContext _simaDb;

        #endregion

        #region Constructors

        public BPRMetricDataSource(SimaContext simaDb)
        {
            _simaDb = simaDb;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<MetricSummary>> GetCortezProcessMetricsAsync()
        {
            return new List<MetricSummary>
            {
                new MetricSummary {DailyValue = 1, MonthToDateValue = 3, Name = "Test metric", Unit = "Int", WeekToDateValue = 2}
            };
        }

        #endregion
    }
}
