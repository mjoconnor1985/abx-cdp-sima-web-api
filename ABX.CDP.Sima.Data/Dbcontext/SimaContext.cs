﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ABX.CDP.Sima.Data.PI;

namespace ABX.CDP.Sima.Data
{
    public class SimaContext: DbContext
    {
        public virtual DbSet<PiTag> PiTags { get; set; }

        public SimaContext(DbContextOptions options): 
            base(options)
        {
        }
    }
}
