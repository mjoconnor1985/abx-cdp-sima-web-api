﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ABX.CDP.Sima.Domain.BPR;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ABX.CDP.BPR.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CortezMetricsController : Controller
    {

        #region Fields

        IBPRMetricService _bprMetricSrv;

        #endregion
        public CortezMetricsController(IBPRMetricService bprMetricSrv)
        {
            _bprMetricSrv = bprMetricSrv;
        }


        [HttpGet]
        [Route("processmetrics")]
        async public Task<IActionResult> GetProcessMetricsAsync()
        {
            var processMetrics = await _bprMetricSrv.GetCortezProcessMetricsAsync();
            return Ok(processMetrics);
        }
    }
}
