﻿using ABX.CDP.Sima.Domain.BPR;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABX.CDP.Sima.WebApi.MappingProfiles
{
    public class MetricSummaryProfile: Profile
    {
        public MetricSummaryProfile()
        {
            CreateMap<ABX.CDP.Sima.Data.BPR.MetricSummary, MetricSummary>();
        }
    }
}
